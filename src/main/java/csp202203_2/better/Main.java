package csp202203_2.better;

import java.util.*;

/**
 * <h1>出行计划 考察点：动态规划、差分法</h1>
 * 差分法，先计算每一阶段的变化，然后求和。差分法的优势在于，只关心每一次发生变化的点
 * 动态规划：阶段性决策问题
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int k = scanner.nextInt();
        int[] result = new int[200001];
        for (int i = 0; i < n; i++) {
            int t = scanner.nextInt();
            int c = scanner.nextInt();
            result[Math.max(t + 1 - k, 0)]--;
            result[Math.max(t - c + 1 - k, 0)]++;
        }
        for (int i = 0; i < 200000; i++) {
            result[i + 1] += result[i];
        }
        for (int i = 0; i < m; i++) {
            System.out.println(result[scanner.nextInt()]);
        }
        scanner.close();
    }
}
