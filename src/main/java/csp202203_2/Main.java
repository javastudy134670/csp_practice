package csp202203_2;

import java.util.Scanner;

/**
 * 出行计划
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int k = scanner.nextInt();
        int[] dp = new int[200001];
        for (int i = 0; i < n; i++) {
            int t = scanner.nextInt();
            int c = scanner.nextInt();
            for (int j = t; j > t - c && j >= k; j--) {
                dp[j - k] += 1;
            }
        }
        for (int i = 0; i < m; i++) {
            int q = scanner.nextInt();
            System.out.println(dp[q]);
        }
    }
}
