package csp202303_1;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Field newField = new Field(0, 0, scanner.nextInt(), scanner.nextInt());
        int result = 0;
        for (int i = 0; i < n; i++) {
            Field field = new Field(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
            if (Field.separate(newField, field)) continue;
            result += Field.calculate(newField, field);
        }
        System.out.println(result);
        scanner.close();
    }
}

class Field {
    int left;
    int right;
    int top;
    int bottom;

    public Field(int left, int bottom, int right, int top) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }

    public static boolean separate(Field a, Field b) {
        return (b.right < a.left || b.left > a.right || a.top < b.bottom || a.bottom > b.top);
    }

    public static int calculate(Field a, Field b) {
        int[] xs = {a.left, a.right, b.left, b.right};
        int[] ys = {a.top, a.bottom, b.top, b.bottom};
        Arrays.sort(xs);
        Arrays.sort(ys);
        return (xs[2] - xs[1]) * (ys[2] - ys[1]);
    }
}