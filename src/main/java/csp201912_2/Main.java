package csp201912_2;

import java.util.*;

/**
 * 垃圾回收
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] result = new int[5];
        List<Row> rows = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Row row = new Row(x);
            int index = rows.indexOf(row);
            if (index == -1) {
                rows.add(row);
            } else {
                row = rows.get(index);
            }
            row.y.add(y);
        }
        rows.sort(Comparator.comparingInt(row -> row.x));
        for (Row row : rows) {
            Collections.sort(row.y);
        }
        boolean start = true;
        for (int i = 0; i < rows.size() - 1; i++) {
            Row row = rows.get(i);
            Row nextRow = rows.get(i + 1);
            if (nextRow.x != row.x + 1) {
                start = true;
                continue;
            }
            if (start) {
                start = false;
                continue;
            }
            Row lastRow = rows.get(i - 1);
            boolean start2 = true;
            for (int j = 0; j < row.y.size() - 1; j++) {
                int y = row.y.get(j);
                int nextY = row.y.get(j + 1);
                if (nextY != y + 1) {
                    start2 = true;
                    continue;
                }
                if (start2) {
                    start2 = false;
                    continue;
                }
                int indexD = Collections.binarySearch(nextRow.y, y, Comparator.naturalOrder());
                int indexU = Collections.binarySearch(lastRow.y, y, Comparator.naturalOrder());
                if (indexU >= 0 && indexD >= 0) {
                    int level = 0;
                    if (indexU > 0 && lastRow.y.get(indexU - 1) == y - 1) level++;
                    if (indexU < lastRow.y.size() - 1 && lastRow.y.get(indexU + 1) == y + 1) level++;
                    if (indexD > 0 && nextRow.y.get(indexD - 1) == y - 1) level++;
                    if (indexD < nextRow.y.size() - 1 && nextRow.y.get(indexD + 1) == y + 1) level++;
                    result[level]++;
                }
            }
        }
        for (int i : result) {
            System.out.println(i);
        }
    }

}

class Row {
    int x;
    List<Integer> y;

    public Row(int x) {
        this.x = x;
        y = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Row) {
            return ((Row) obj).x == x;
        }
        if (obj instanceof Integer) {
            return x == (Integer) obj;
        }
        return super.equals(obj);
    }
}