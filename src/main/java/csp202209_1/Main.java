package csp202209_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int a = scanner.nextInt();
            System.out.printf("%d ", m % a);
            m /= a;
        }
        System.out.println();
    }
}
