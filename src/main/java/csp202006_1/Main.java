package csp202006_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 如果只是检查符号是否相等的话，不要用整数乘法，可能会溢出。可以直接符号化，或者用浮点乘法（不过较慢）
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        List<Point> A = new ArrayList<>();
        List<Point> B = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            Point point = new Point(scanner.nextInt(), scanner.nextInt());
            String type = scanner.next();
            if (type.equals("A")) {
                A.add(point);
            } else {
                B.add(point);
            }
        }
        for (int i = 0; i < m; i++) {
            int t1 = scanner.nextInt();
            int t2 = scanner.nextInt();
            int t3 = scanner.nextInt();
            if (verify(t1, t2, t3, A, B)) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
        scanner.close();
    }

    static boolean verify(int theta1, int theta2, int theta3, List<Point> A, List<Point> B) {
        int a = result(theta1, theta2, theta3, A.get(0));
        for (Point point : A) {
            if (a * result(theta1, theta2, theta3, point) <= 0) return false;
        }
        for (Point point : B) {
            if (a * result(theta1, theta2, theta3, point) >= 0) return false;
        }
        return true;
    }

    static int result(int theta1, int theta2, int theta3, Point point) {
        return flag(theta1 + point.x * theta2 + point.y * theta3);
    }

    static int flag(int num) {
        return Integer.compare(num, 0);
    }
}

class Point {
    int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
