package csp202309_2;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        double[] k = new double[n + 1];
        double[] theta = new double[n + 1];
        k[0] = 1;
        theta[0] = 0;
        for (int i = 0; i < n; i++) {
            if (scanner.nextInt() == 1) {
                k[i + 1] = k[i] * scanner.nextDouble();
                theta[i + 1] = theta[i];
            } else {
                k[i + 1] = k[i];
                theta[i + 1] = theta[i] + scanner.nextDouble();
            }
        }
        for (int i0 = 0; i0 < m; i0++) {
            int i = scanner.nextInt();
            int j = scanner.nextInt();
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            double a = k[j] / k[i - 1];
            double b = theta[j] - theta[i - 1];
            double sb = Math.sin(b);
            double cb = Math.cos(b);
            System.out.printf("%f %f%n", a * (x * cb - y * sb), a * (x * sb + y * cb));
        }
        scanner.close();
    }
}
