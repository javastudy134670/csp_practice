package csp201812_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int r = scanner.nextInt();
        scanner.nextInt();
        scanner.nextInt();
        int n = scanner.nextInt();
        long res = 0;
        for (int i = 0; i < n; i++) {
            int key = scanner.nextInt();
            int t = scanner.nextInt();
            if (key != 3) {
                res += t;
            }
            if (key == 2) {
                res += r;
            }
        }
        System.out.println(res);
        scanner.close();
    }
}
