package csp202104_2;

import java.util.Scanner;

/**
 * <h1>领域均值 考察点：二维数组中，求特定矩形区域元素之和。 差分法</h1>
 * 二维范围内求和的典型问题
 * 数组特定区间求和问题，要多次在特定区间内求和，千万不要暴力求和，而是在数组形成的过程把局部总和求出来，这个会使求和问题更方便
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int L = scanner.nextInt();
        int r = scanner.nextInt();
        int t = scanner.nextInt();
        int[][] array = new int[n][];
        for (int i = 0; i < n; i++) {
            array[i] = new int[n + 1];
            int sum = 0;
            for (int j = 0; j < n; j++) {
                sum += scanner.nextInt();
                array[i][j + 1] = sum;
            }
        }
        int result = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int left = Math.max(i - r, 0);
                int right = Math.min(i + r, n - 1);
                int top = Math.min(j + r, n - 1);
                int bottom = Math.max(j - r, 0);
                int sum = 0;
                for (int k = bottom; k <= top; k++) {
                    sum += array[k][right + 1] - array[k][left];
                }
                if (sum <= t * (top - bottom + 1) * (right - left + 1)) {
                    result += 1;
                }
            }
        }
        System.out.println(result);
        scanner.close();
    }
}
