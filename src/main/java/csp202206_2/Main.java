package csp202206_2;

import java.util.*;

/**
 * <h1>寻宝大冒险 考点：对输入坐标的转化、匹配比较</h1>
 * 本题难度相对较低，要注意输入坐标顺序不等于数组输入顺序。
 */
public class Main {
    static int result;
    static int n;
    static int L;
    static int S;

    static {
        result = 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<Point> trees = new HashSet<>();
        n = scanner.nextInt();
        L = scanner.nextInt();
        S = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            trees.add(new Point(scanner.nextInt(), scanner.nextInt()));
        }
        int[][] diagram = new int[S + 1][];
        for (int i = S; i >= 0; i--) {
            diagram[i] = new int[S + 1];
            for (int j = 0; j < S + 1; j++) {
                diagram[i][j] = scanner.nextInt();
            }
        }
        for (Point tree : trees) {
            scan(diagram, trees, tree.x, tree.y);
        }
        System.out.println(result);
    }

    static void scan(int[][] diagram, Set<Point> tress, int i, int j) {
        if (i > L - S || j > L - S) return;
        for (int a = 0; a <= S; a++) {
            for (int b = 0; b <= S; b++) {
                boolean x = (diagram[a][b] == 1);
                boolean y = (tress.contains(new Point(a + i, b + j)));
                if (x && !y || !x && y) return;
            }
        }
        result += 1;
    }
}

class Point {
    int x;
    int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
