package csp202104_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int L = scanner.nextInt();
        int[] results = new int[L];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int tmp = scanner.nextInt();
                results[tmp]+=1;
            }
        }
        for (int result : results) {
            System.out.printf("%d ",result);
        }
        scanner.close();
    }
}
