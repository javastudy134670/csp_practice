package csp202212_2;

import java.util.*;

/**
 * <h1>训练计划  考察点：动态规划、最早开始时间、最晚开始时间、图的邻接表表示</h1>
 * 解决这类问题，首先要学会使用邻接表。
 * 最早开始时间，计算相对简单。从前往后推算。每个头节点（没有依赖的点）遍历后继，累积相加就可以了。因为这是一棵树，每个点的前继最多为1
 * 但是最晚开始时间，需要从后往前算。每个点的后继不止一个，因此会比较麻烦，需要比较出最晚的后继点，然后减去所需时间。
 *
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int deadline = scanner.nextInt();
        int subject = scanner.nextInt();
        List<Node> nodes = new ArrayList<>();//所有的节点
        List<Node> heads = new ArrayList<>();//所有的头节点
        for (int i = 0; i < subject; i++) {
            Node node = new Node();
            nodes.add(node);
            int parent = scanner.nextInt();
            if (parent == 0) {
                heads.add(node);
            } else {
                nodes.get(parent - 1).next.add(node);
            }
        }
        for (Node node : nodes) {
            node.cost = scanner.nextInt();
        }
        int[] early = new int[subject];
        for (Node head : heads) {
            earlyTime(early, 1, head, nodes);
        }
        for (int each : early) {
            System.out.printf("%d ", each);
        }
        System.out.println();
        int[] last = new int[subject];
        boolean flag = true;
        for (int i = 0; i < subject; i++) {
            last[i] = lastTime(last, nodes.get(i), nodes, deadline);
            if (last[i] <= 0) flag = false;
        }
        if (flag) {
            for (int each : last) {
                System.out.printf("%d ", each);
            }
        }
        System.out.println();
    }

    /**
     * 计算最早开始时间
     * @param early 最早开始时间列表，用于储存计算结果。表示nodes中第i个项目的最早开始时间
     * @param thisDay 当前日期
     * @param node 当前训练项目
     * @param nodes 所有训练项目。这里是为了获得训练项目的下标
     */
    static void earlyTime(int[] early, int thisDay, Node node, List<Node> nodes) {
        early[nodes.indexOf(node)] = thisDay;
        if (node.next == null) return;
        for (Node node1 : node.next) {
            earlyTime(early, thisDay + node.cost, node1, nodes);
        }
    }

    /**
     * 计算最晚开始时间
     * @param last 最晚开始时间列表，用于储存结果
     * @param node 当前训练项目
     * @param nodes 所有训练项目
     * @param deadline deadline
     * @return 最晚开始时间，用于比较
     */
    static int lastTime(int[] last, Node node, List<Node> nodes, int deadline) {
        int index = nodes.indexOf(node);
        if (last[index] != 0) {
            return last[index];
        }
        if (node.next.isEmpty()) {
            last[index] = deadline - node.cost + 1;
            return last[index];
        }
        int min = Integer.MAX_VALUE;
        for (Node each : node.next) {
            int time = lastTime(last, each, nodes, deadline);//选出后继的最晚的最晚开始时间
            if (time < min) min = time;
        }
        last[index] = min - node.cost;
        return last[index];
    }
}

class Node {
    List<Node> next;//这里用HashSet也是可以的
    int cost;

    public Node() {
        this.cost = 0;
        this.next = new ArrayList<>();
    }
}
