package csp202305_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Condition> conditionList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < 8; j++) {
                builder.append(scanner.next());
            }
            Condition condition=new Condition(builder.toString());
            int index = conditionList.indexOf(condition);
            if (index == -1) {
                conditionList.add(condition);
                System.out.println(1);
            }else{
                conditionList.get(index).num++;
                System.out.println(conditionList.get(index).num);
            }
        }
        scanner.close();
    }
}
class Condition {
    String condition;
    int num;

    @Override
    public String toString() {
        return condition;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof  Condition){
            return condition.equals(obj.toString());
        }
        return false;
    }

    Condition(String condition) {
        this.condition = condition;
        num = 1;
    }
}
