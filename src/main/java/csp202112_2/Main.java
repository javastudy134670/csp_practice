package csp202112_2;

import java.util.Scanner;

/**
 * <h1>序列查询新解 考察点：类似于差分法</h1>
 * 注意两个序列的变化节点，两个发生变化节点之间是可以用乘法的。关键就是找到所有这样的节点之间的宽度。
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int N = scanner.nextInt();
        int r = N / (n + 1);
        long sum = 0;
        int[] numbers = new int[n + 2];
        for (int i = 1; i < n + 1; i++) {
            numbers[i] = scanner.nextInt();
        }
        numbers[n + 1] = N;
        for (int i = 0; i < n + 1; i++) {
            int tmpSum = 0;
            int length;
            for (int j = numbers[i]; j < numbers[i + 1]; j += length) {
                int numEnd = ((j / r) + 1) * r - 1;
                if (numEnd >= numbers[i + 1]) {
                    numEnd = numbers[i + 1] - 1;
                }
                length = numEnd - j + 1;
                tmpSum += Math.abs(j / r - i) * length;
            }
            sum += tmpSum;
        }
        System.out.println(sum);
    }
}
