package csp201803_2;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int L = scanner.nextInt();
        int t = scanner.nextInt();
        DoubleIndex[] doubleIndex1 = new DoubleIndex[n];
        DoubleIndex[] doubleIndex2 = new DoubleIndex[n];
        for (int i = 0; i < n; i++) {
            int tmp = scanner.nextInt();
            doubleIndex1[i]= new DoubleIndex(tmp,i);
            doubleIndex2[i]=new DoubleIndex(L-Math.abs((tmp+t)%(2*L)-L),0);
        }
        Arrays.sort(doubleIndex1, Comparator.comparingInt(doubleIndex-> doubleIndex.value));
        Arrays.sort(doubleIndex2,Comparator.comparingInt(doubleIndex->doubleIndex.value));
        for (int i = 0; i < n; i++) {
            doubleIndex2[i].index=doubleIndex1[i].index;
        }
        Arrays.sort(doubleIndex2,Comparator.comparingInt(doubleIndex-> doubleIndex.index));
        for (int i = 0; i < n; i++) {
            System.out.printf("%d ",doubleIndex2[i].value);
        }
        scanner.close();
    }
}

class DoubleIndex {
    int value;
    int index;
    DoubleIndex(int value, int index){
        this.value=value;
        this.index=index;
    }
}
