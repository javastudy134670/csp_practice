package csp202209_2;

import java.util.Scanner;

/**
 * <h1>背包问题 考察点：动态规划</h1>
 * 阶段性决策，求最值，常常会选择动态规划。
 * 对于每一个阶段求最值，可以寻找上一阶段的不同决策的最值结果与和本阶段最值结果之间的关系，然后使用递归的方式求解问题。
 * 注意，动态规划一定有一个边界，也就是问题的最初阶段。在本题，问题的最初阶段就是，没有考虑任何书时，能省下来的钱一定时0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int volume = scanner.nextInt();
        int total = 0;
        int[] value = new int[n];
        for (int i = 0; i < n; i++) {
            value[i] = scanner.nextInt();
            total += value[i];
        }
        int[][] dp = new int[n + 1][];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = new int[total + 1];
        }
        System.out.println(total - maxDelete(n, total - volume, dp, value));
    }

    static int maxDelete(int j, int canDelete, int[][] dp, int[] value) {
        if (canDelete < 0) return Integer.MIN_VALUE;
        if (j == 0) return 0;
        if (dp[j][canDelete] == 0) {
            int a = maxDelete(j - 1, canDelete, dp, value);
            int b = maxDelete(j - 1, canDelete - value[j - 1], dp, value) + value[j - 1];
            dp[j][canDelete] = Math.max(a, b);
        }
        return dp[j][canDelete];
    }
}