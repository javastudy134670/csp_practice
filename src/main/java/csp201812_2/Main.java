package csp201812_2;

import java.util.Scanner;

/**
 * 假设从绿灯开始算，然后是黄灯，红灯。
 * 初始剩余时间 still计算：绿灯=t+y+r, 黄=t+r ,红=t
 * 耗费时间=res
 * 参考数 m=(res-still)%(g+y+r)
 * [0,g) 可通行 [g,y+g+r) 等待 等待时间y+g+r-m;//存疑
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int r = scanner.nextInt();
        int y = scanner.nextInt();
        int g = scanner.nextInt();
        int c = r + y + g;
        int n = scanner.nextInt();
        long res = 0;
        for (int i = 0; i < n; i++) {
            int key = scanner.nextInt();
            int t = scanner.nextInt();
            if (key == 0) {
                res += t;
                continue;
            }
            int still = t;
            switch (key) {
                case 3:
                    still += y;
                case 2:
                    still += r;
            }
            long m = c - (c + res - still) % c;
            if (m <= r + y) {
                res += m;
            }
        }
        System.out.println(res);
        scanner.close();
    }
}
