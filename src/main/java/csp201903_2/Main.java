package csp201903_2;

import java.util.Scanner;

/**
 * 二十四点
 * 字符串处理
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            StringScanner stringScanner = new StringScanner(scanner.next());
            int result = stringScanner.scanElement();
            char ch = ' ';
            if (!stringScanner.closed()) {
                ch = stringScanner.currentChar();
            }
            while (ch != ' ') {
                stringScanner.ptr++;
                if (ch == '+') {
                    result += stringScanner.scanElement();
                } else {
                    result -= stringScanner.scanElement();
                }
                if (stringScanner.closed()) break;
                ch = stringScanner.currentChar();
            }
            if (result == 24) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
        scanner.close();
    }
}

class StringScanner {
    final String str;
    int ptr;

    StringScanner(String str) {
        this.str = str;
        ptr = 0;
    }

    int scanInt() {
        int j = ptr;
        while (j < str.length() && charInNumber(str.charAt(j))) j++;
        String substring = str.substring(ptr, j);
        ptr = j;
        return Integer.parseInt(substring);
    }

    int scanElement() {
        int elem = scanInt();
        char ch = ' ';
        if (!closed()) {
            ch = currentChar();
        }
        while (ch == 'x' || ch == '/') {
            ptr++;
            if (ch == 'x') {
                elem *= scanInt();
            } else {
                elem /= scanInt();
            }
            if (closed()) break;
            ch = currentChar();
        }
        return elem;
    }

    boolean closed() {
        return ptr >= str.length();
    }

    char currentChar() {
        if (closed()) throw new RuntimeException("Has been closed");
        return str.charAt(ptr);
    }

    static boolean charInNumber(char ch) {
        return ch >= '0' && ch <= '9';
    }
}
