package csp202206_3;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Serialization roles = new Serialization();
        Map<String, BitSet> option = new HashMap<>();
        Map<String, BitSet> resourceType = new HashMap<>();
        Map<String, BitSet> resourceName = new HashMap<>();
        Map<String, BitSet> users = new HashMap<>();
        Map<String, BitSet> groups = new HashMap<>();
        option.put("*", new BitSet());
        resourceType.put("*", new BitSet());
        resourceName.put("*", new BitSet());

        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int q = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int r = roles.get(scanner.next());
            capture(scanner.nextInt(), scanner, option, r);
            capture(scanner.nextInt(), scanner, resourceType, r);
            int nn = scanner.nextInt();
            if (nn > 0) {
                capture(nn, scanner, resourceName, r);
            } else {
                resourceName.get("*").set(r);
            }

        }
        for (int i = 0; i < m; i++) {
            int c = roles.get(scanner.next());
            int ns = scanner.nextInt();
            for (int j = 0; j < ns; j++) {
                String type = scanner.next();
                String name = scanner.next();
                if (type.equals("u")) {
                    if (!users.containsKey(name)) {
                        users.put(name, new BitSet());
                    }
                    users.get(name).set(c);
                } else {
                    if (!groups.containsKey(name)) {
                        groups.put(name, new BitSet());
                    }
                    groups.get(name).set(c);
                }
            }
        }
        option.values().forEach(set -> set.or(option.get("*")));
        resourceType.values().forEach(set -> set.or(resourceType.get("*")));
        resourceName.values().forEach(set -> set.or(resourceName.get("*")));
        for (int i = 0; i < q; i++) {
            String userName = scanner.next();
            int ng = scanner.nextInt();
            BitSet set = new BitSet();
            if (users.containsKey(userName)) {
                set.or(users.get(userName));
            }
            for (int j = 0; j < ng; j++) {
                String group = scanner.next();
                if (groups.containsKey(group)) {
                    set.or(groups.get(group));
                }
            }
            String opt = scanner.next();
            if (option.containsKey(opt)) {
                set.and(option.get(opt));
            } else {
                set.and(option.get("*"));
            }
            String type = scanner.next();
            if (resourceType.containsKey(type)) {
                set.and(resourceType.get(type));
            } else {
                set.and(resourceType.get("*"));
            }
            String name = scanner.next();
            if (resourceName.containsKey(name)) {
                set.and(resourceName.get(name));
            } else {
                set.and(resourceName.get("*"));
            }
            if (set.isEmpty()) {
                System.out.println(0);
            } else {
                System.out.println(1);
            }
        }
        scanner.close();
    }

    private static void capture(int nvo, Scanner scanner, Map<String, BitSet> option, int c) {
        for (int j = 0; j < nvo; j++) {
            String name = scanner.next();
            if (!option.containsKey(name)) {
                option.put(name, new BitSet());
            }
            option.get(name).set(c);
        }
    }
}

class Serialization {
    Map<String, Integer> map = new HashMap<>();
    int size = 0;

    int get(String key) {
        if (!map.containsKey(key)) {
            map.put(key, size);
            size++;
        }
        return map.get(key);
    }
}
