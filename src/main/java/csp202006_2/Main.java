package csp202006_2;

import java.util.*;

/**
 * 稀疏向量内积计算
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        Map<Integer, Long> map = new HashMap<>();
        for (int i = 0; i < a; i++) {
            map.put(scanner.nextInt(), scanner.nextLong());
        }
        long sum = 0;
        for (int i = 0; i < b; i++) {
            int pos = scanner.nextInt();
            int value = scanner.nextInt();
            if (map.containsKey(pos)) {
                sum += map.get(pos) * value;
            }
        }
        System.out.println(sum);
        scanner.close();
    }
}
