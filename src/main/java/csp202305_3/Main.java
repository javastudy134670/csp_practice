package csp202305_3;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < n; i += 8) {
            stringBuilder.append(scanner.next());
        }
        String input = stringBuilder.toString();
        int[] inputBytes = hex2Bytes(input);
        Integer[] outputBytes = release(inputBytes);
        byte2Hex(outputBytes);
        scanner.close();
    }

    static int[] hex2Bytes(String hex) {
        int n = hex.length() / 2;
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            String number = hex.substring(2 * i, 2 * i + 2);
            result[i] = Integer.parseInt(number, 16);
        }
        return result;
    }

    static void byte2Hex(Integer[] data) {
        for (int i = 0; i < data.length; i++) {
            String str = Integer.toHexString(data[i]);
            if (str.length() < 2) System.out.print("0");
            System.out.print(Integer.toHexString(data[i]));
            if (i % 8 == 7) System.out.println();
        }
    }

    static Integer[] release(int[] inputBytes) {
        int i = 0;
        int cur = inputBytes[i];
        long len;
        Stack<Integer> stack = new Stack<>();
        while (cur >= 128) {
            stack.push(cur);
            cur = inputBytes[i + 1];
            i++;
        }
        len = cur;
        while (!stack.empty()) {
            len = len * 128 + stack.pop() - 128;
        }
        List<Integer> result = new ArrayList<>();
        while (i + 1 < inputBytes.length) {
            //先取数再处理
            cur = inputBytes[i + 1];
            i++;
            switch (cur & 0b11) {
                case 0b00:
                    //字面量
                    int size = 1 + ((cur & 0xfc) >> 2);
                    if (size > 60) {
                        stack.clear();
                        for (int j = 0; j < size - 60; j++) {
                            cur = inputBytes[i + 1];
                            i++;
                            stack.push(cur);
                        }
                        size = 0;
                        while (!stack.empty()) {
                            size <<= 8;
                            size |= stack.pop();
                        }
                        size += 1;
                    }
                    for (int j = 0; j < size; j++) {
                        cur = inputBytes[i + 1];
                        i++;
                        result.add(cur);
                    }
                    break;
                case 0b01:
                    //回溯形式一
                    int l = ((cur & 0x1c) >> 2) + 4;
                    int o = (cur & 0xe0) << 3;
                    cur = inputBytes[i + 1];
                    i += 1;
                    o |= cur;
                    int p = result.size();
                    for (int j = 0; j < l; j++) {
                        result.add(result.get(p - o + j % o));
                    }
                    break;
                case 0b10:
                    //回溯形式二
                    l = ((cur & 0xfc) >> 2) + 1;
                    cur = inputBytes[i + 1];
                    i++;
                    o = cur;
                    cur = inputBytes[i + 1];
                    i++;
                    o |= cur << 8;
                    p = result.size();
                    for (int j = 0; j < l; j++) {
                        result.add(result.get(p - o + j % o));
                    }
                    break;
                default:
                    deadLoop();

            }
        }//while
        return result.toArray(new Integer[1]);
    }

    static void deadLoop() {
        int i = 1;
        int j = 1;
        while (i == j) {
            i++;
            j++;
        }
    }
}
