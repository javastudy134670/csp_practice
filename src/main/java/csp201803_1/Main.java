package csp201803_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int score = 0;
        int tmp = scanner.nextInt();
        int combo = 0;
        while (tmp!=0){
            if(tmp==1){
                combo=0;
                score+=1;
            }else{
                combo++;
                score+=2*combo;
            }
            tmp= scanner.nextInt();
        }
        System.out.println(score);
        scanner.close();
    }
}
