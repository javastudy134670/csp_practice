import java.math.BigInteger;
import java.util.*;

public class Test {
    public static void main(String[] args) {
        new Solution().findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4});
    }
}
class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int l1 = 0;
        int l2=0;
        boolean same = (nums1.length+nums2.length)%2==1;
        int k = (nums1.length+nums2.length-1)/2;
        while(k>1){
            int ks = k/2;
            int a = nums1[l1+ks-1];
            int b=nums2[l2+ks-1];
            if(a<b){
                l1=l1+ks;
            }else{
                l2=l2+ks;
            }
            k=k-ks;
        }
        ArrayList<Integer> list = new ArrayList<>();
        list.add(nums1[l1]);
        list.add(nums2[l2]);
        if(l1+1<nums1.length) list.add(nums1[l1+1]);
        if(l2+1<nums2.length) list.add(nums2[l2+1]);
        if(l1+2<nums1.length) list.add(nums1[l1+2]);
        if(l2+2<nums2.length) list.add(nums2[l2+2]);
        list.sort(Comparator.naturalOrder());
        if(same) return list.get(1);
        return (list.get(1)+list.get(2))/2.0;
    }
}

class Point {
    final int x;
    final int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override

    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point point = (Point) obj;
            return x == point.x && y == point.y;
        }
        return super.equals(obj);
    }
}
