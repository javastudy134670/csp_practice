package csp202012_2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * 还是用差分法
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int numOne = 0;
        List<Data> datalist = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            Data data = new Data(scanner.nextInt(), scanner.nextInt());
            datalist.add(data);
            if (data.perdition == 1) numOne += 1;
        }
        datalist.sort(Comparator.comparingInt(data -> data.v));
        int max = numOne;
        Data best = datalist.get(0);
        int right = numOne;
        for (int i = 1; i < datalist.size(); i++) {
            if (datalist.get(i - 1).perdition == 0) {
                right += 1;
            } else {
                right -= 1;
            }
            if (datalist.get(i).v == datalist.get(i - 1).v) {
                if (best == datalist.get(i - 1)) {
                    best = datalist.get(i);
                }
            } else if (right >= max) {
                max = right;
                best = datalist.get(i);
            }
        }
        System.out.println(best.v);
        scanner.close();
    }
}

class Data {
    int v;
    int perdition;

    public Data(int v, int perdition) {
        this.v = v;
        this.perdition = perdition;
    }

    @Override
    public String toString() {
        return String.format("{v: %d, perdition: %d}", v, perdition);
    }
}