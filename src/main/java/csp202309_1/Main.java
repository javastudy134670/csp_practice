package csp202309_1;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int x = 0;
        int y = 0;
        for (int i = 0; i < n; i++) {
            x += scanner.nextInt();
            y += scanner.nextInt();
        }
        for (int i = 0; i < m; i++) {
            System.out.printf("%d %d%n", scanner.nextInt() + x, scanner.nextInt() + y);
        }
        scanner.close();
    }
}
