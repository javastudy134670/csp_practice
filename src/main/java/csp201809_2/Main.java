package csp201809_2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[2 * n];
        int[] b = new int[2 * n];
        for (int i = 0; i < 2 * n; i++) {
            a[i] = scanner.nextInt();
        }
        for (int i = 0; i < 2 * n; i++) {
            b[i] = scanner.nextInt();
        }
        int i = 0;
        int j = 0;
        int result = 0;
        while (i < n && j < n) {
            if (a[2 * i + 1] <= b[2 * j]) {
                while (i < n && a[2 * i + 1] <= b[2 * j]) i++;
                if (i >= n) break;
                continue;
            }
            if (a[2 * i] >= b[2 * j + 1]) {
                while (j < n && a[2 * i] >= b[2 * j + 1]) j++;
                if (j >= n) break;
                continue;
            }
            result += Math.min(a[2 * i + 1], b[2 * j + 1]) - Math.max(a[2 * i], b[2 * j]);
            if (a[2 * i + 1] > b[2 * j + 1]) {
                j++;
            } else {
                i++;
            }
        }
        System.out.println(result);
        scanner.close();
    }
}
