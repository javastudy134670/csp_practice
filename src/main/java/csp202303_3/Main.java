package csp202303_3;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<User> users = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            User user = new User(scanner.nextInt());
            users.add(user);
            int m = scanner.nextInt();
            for (int j = 0; j < m; j++) {
                user.attributes.put(scanner.nextInt(), scanner.nextInt());
            }
        }
        int m = scanner.nextInt();
        for (int i = 0; i < m; i++) {
            StringScanner stringScanner = new StringScanner(scanner.next());
            List<Combination> DNF = stringScanner.scanDNF();
            List<User> userList = new ArrayList<>();
            for (User user : users) {
                for (Combination combination : DNF) {
                    if (user.verify(combination)) {
                        userList.add(user);
                        break;
                    }
                }
            }
            userList.sort(Comparator.comparingInt(user -> user.DN));
            for (User user : userList) {
                System.out.printf("%d ", user.DN);
            }
            System.out.println();
        }

        scanner.close();
    }
}

class User {
    int DN;
    Map<Integer, Integer> attributes = new HashMap<>();

    public User(int DN) {
        this.DN = DN;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User) {
            return DN == ((User) obj).DN;
        }
        return super.equals(obj);
    }

    public boolean verify(Combination combination) {
        for (Attribute attribute : combination) {
            int key = attribute.attribute;
            if (!attributes.containsKey(key)) return false;
            int value = attributes.get(key);
            if (attribute.positive != attribute.values.contains(value)) {
                return false;
            }
        }
        return true;
    }
}

class Attribute {
    int attribute;
    List<Integer> values = new ArrayList<>();
    boolean positive;

    public Attribute(int attribute, int value, boolean positive) {
        this.attribute = attribute;
        values.add(value);
        this.positive = positive;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Attribute) {
            Attribute attribute1 = (Attribute) obj;
            return attribute1.attribute == this.attribute;
        }
        return super.equals(obj);
    }
}

class Combination extends ArrayList<Attribute> {
    boolean NO = false;

    @Override
    public boolean add(Attribute attribute) {
        if (NO) return false;
        return super.add(attribute);
    }

    public void embrace(Attribute attribute) {
        if (NO) return;
        int index = indexOf(attribute);
        if (index == -1) {
            add(attribute);
        } else {
            Attribute attribute1 = get(index);
            if (!attribute1.positive && !attribute.positive) {
                for (Integer value : attribute.values) {
                    if (!attribute1.values.contains(value)) attribute1.values.add(value);
                }
            } else if (attribute.positive) {
                if (attribute1.positive && (!attribute1.values.get(0).equals(attribute.values.get(0)))) {
                    NO = true;//错误
                    return;
                }
                set(index, attribute);
            }

        }
    }

    public static Combination multi(Combination combination1, Combination combination2) {
        Combination combination = new Combination();
        if (combination1.NO || combination2.NO) {
            combination.NO = true;
        } else {
            combination.addAll(combination1);
            for (Attribute attribute : combination2) {
                combination.embrace(attribute);
            }
        }
        return combination;
    }
}

class StringScanner {
    String str;
    int offset;

    public StringScanner(String str) {
        this.str = str;
        offset = 0;
    }

    public int length() {
        return str.length();
    }

    public char charAt(int i) {
        return str.charAt(i);
    }

    public boolean reachEnd() {
        return offset >= length();
    }

    public char top() {
        if (reachEnd()) throw new RuntimeException("reached end");
        return charAt(offset);
    }

    public void next() {
        if (reachEnd()) throw new RuntimeException("reached end");
        offset++;
    }

    public int scanInt() {
        int i = offset;
        while (!reachEnd() && isDigit(top())) next();
        return Integer.parseInt(str.substring(i, offset));
    }

    public Attribute scanBaseExpr() {
        int a = scanInt();
        char ch = top();
        next();
        int b = scanInt();
        return new Attribute(a, b, ch == ':');
    }

    public List<Combination> scanDNF() {
        char ch = top();
        if (ch == '|') {
            next();
            next();
            List<Combination> list1 = scanDNF();
            next();
            next();
            List<Combination> list2 = scanDNF();
            next();
            list1.addAll(list2);
            return list1;
        } else if (ch == '&') {
            next();
            next();
            List<Combination> list1 = scanDNF();
            next();
            next();
            List<Combination> list2 = scanDNF();
            next();
            List<Combination> list = new ArrayList<>();
            for (Combination combination1 : list1) {
                for (Combination combination2 : list2) {
                    Combination combination = Combination.multi(combination1, combination2);
                    if (!combination.NO) list.add(combination);
                }
            }
            return list;
        } else {
            List<Combination> list = new ArrayList<>();
            Combination combination = new Combination();
            combination.embrace(scanBaseExpr());
            list.add(combination);
            return list;
        }
    }

    static boolean isDigit(char ch) {
        return ch >= '0' && ch <= '9';
    }
}