package csp202303_3.rewrite;

import java.util.*;

public class Main {
    static int[] DNs; //最多2500个用户
    static Map<Integer, Map<Integer, BitSet>> usersThatIndexAndValue = new HashMap<>();
    static Map<Integer, BitSet> usersThatIndex = new HashMap<>();
    static final BitSet nullSet = new BitSet();

    public static void addForUsersThatIndexAndValue(int attribute, int key, int value) {
        if (!usersThatIndexAndValue.containsKey(attribute)) {
            usersThatIndexAndValue.put(attribute, new HashMap<>());
        }
        if (!usersThatIndexAndValue.get(attribute).containsKey(key)) {
            usersThatIndexAndValue.get(attribute).put(key, new BitSet());
        }
        usersThatIndexAndValue.get(attribute).get(key).set(value);
    }

    public static void addForUsersThatIndex(int key, int value) {
        if (!usersThatIndex.containsKey(key)) {
            usersThatIndex.put(key, new BitSet());
        }
        usersThatIndex.get(key).set(value);
    }

    public static BitSet getFromUsersThatIndexAndValue(int attribute, int key) {
        if (!usersThatIndexAndValue.containsKey(attribute) || !usersThatIndexAndValue.get(attribute).containsKey(key)) {
            return nullSet;
        }
        return usersThatIndexAndValue.get(attribute).get(key);
    }

    public static BitSet getFromUsersThatIndex(int key) {
        if (!usersThatIndex.containsKey(key)) {
            return nullSet;
        }
        return usersThatIndex.get(key);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        DNs = new int[n];
        for (int i = 0; i < n; i++) {
            DNs[i] = scanner.nextInt();
            int m = scanner.nextInt();
            for (int j = 0; j < m; j++) {
                int attribute = scanner.nextInt();
                int key = scanner.nextInt();
                addForUsersThatIndexAndValue(attribute, key, i);
                addForUsersThatIndex(attribute, i);
            }
        }
        int m = scanner.nextInt();
        StringScanner stringScanner = new StringScanner("");
        BitSet set = new BitSet();
        List<Integer> ans = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            stringScanner.rewind(scanner.next());
            set.clear();
            ans.clear();
            stringScanner.scanExpr(set, false);
            set.stream().forEach(each -> ans.add(DNs[each]));
            ans.sort(Comparator.naturalOrder());
            ans.forEach(each -> System.out.printf("%d ", each));
            System.out.println();
        }
        scanner.close();
    }
}

class StringScanner {
    String str;
    int offset;

    public StringScanner(String str) {
        rewind(str);
    }

    public void rewind(String str) {
        this.str = str;
        offset = 0;
    }

    public int length() {
        return str.length();
    }

    public char charAt(int i) {
        return str.charAt(i);
    }

    public boolean reachEnd() {
        return offset >= length();
    }

    public char top() {
        if (reachEnd()) throw new RuntimeException("reached end");
        return charAt(offset);
    }

    public void next() {
        if (reachEnd()) throw new RuntimeException("reached end");
        offset++;
    }

    public int scanInt() {
        int i = offset;
        while (!reachEnd() && isDigit(top())) next();
        return Integer.parseInt(str.substring(i, offset));
    }

    public void scanBaseExpr(BitSet set, boolean combination) {
        int a = scanInt();
        char ch = top();
        next();
        int b = scanInt();
        BitSet thatIndexAndValue = Main.getFromUsersThatIndexAndValue(a, b);
        BitSet thatIndex = Main.getFromUsersThatIndex(a);
        if (combination) {
            if (ch == ':') {
                set.and(thatIndexAndValue);
            } else {
                set.and(thatIndex);
                set.andNot(thatIndexAndValue);
            }
        } else if (ch == ':') {
            set.or(thatIndexAndValue);
        } else {//A+(B~C)
            BitSet tmp = (BitSet) thatIndex.clone();
            tmp.andNot(thatIndexAndValue);
            set.or(tmp);
        }
    }

    public void scanExpr(BitSet set, boolean combination) {
        switch (top()) {
            case '&':
                next();
                next();
                if (combination) {
                    scanExpr(set, true);
                    next();
                    next();
                    scanExpr(set, true);
                } else {
                    BitSet tmp = new BitSet();
                    scanExpr(tmp, false);
                    next();
                    next();
                    scanExpr(tmp, true);
                    set.or(tmp);
                }
                next();
                break;
            case '|':
                next();
                next();
                if (combination) {
                    BitSet tmp = new BitSet();
                    scanExpr(tmp, false);
                    next();
                    next();
                    scanExpr(tmp, false);
                    set.and(tmp);
                } else {
                    scanExpr(set, false);
                    next();
                    next();
                    scanExpr(set, false);
                }
                next();
                break;
            default:
                scanBaseExpr(set, combination);
        }
    }

    static boolean isDigit(char ch) {
        return ch >= '0' && ch <= '9';
    }

    @Override
    public String toString() {
        return String.format("offset:%d, str:%s", offset, str);
    }
}