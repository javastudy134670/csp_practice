package csp201912_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] p = new int[4];
        int num = 1;
        for (int i = 0; i < n; i++) {
            if (jud(num) || num % 7 == 0) {
                p[(num-1) % 4] += 1;
                n++;
            }
            num++;
        }
        for (int i : p) {
            System.out.println(i);
        }
        scanner.close();
    }
    static boolean jud(int n){
        while(n>0){
            if(n%10==7) {
                return true;
            }
            n/=10;
        }
        return false;
    }
}
