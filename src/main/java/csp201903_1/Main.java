package csp201903_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int max = Math.max(arr[0], arr[n - 1]);
        int min = Math.min(arr[0], arr[n - 1]);
        int mid_2;
        if(n%2==0){
            mid_2 = (arr[n / 2] + arr[(n -1 ) / 2]);
        }else{
            mid_2 = (arr[n/2])*2;
        }
        if (mid_2%2!=0) {
            System.out.printf("%d %.1f %d%n", max, (double)mid_2/2, min);
        } else {
            System.out.printf("%d %d %d%n", max, mid_2/2, min);
        }
        scanner.close();
    }
}
