package csp201909_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();  //苹果个数
        List<Integer> fallingTree = new ArrayList<>();
        int T = 0;  //总数
        int D;  //棵树
        int E = 0;  //组数
        for (int i = 0; i < N; i++) {
            int m = scanner.nextInt();
            int apples = 0;
            boolean fall = false;
            for (int j = 0; j < m; j++) {
                int tmp = scanner.nextInt();
                if (tmp > 0) {
                    if (tmp < apples) fall = true;
                    apples = tmp;
                } else {
                    apples += tmp;
                }
            }
            T += apples;
            if (fall) {
                fallingTree.add(i);
            }
        }

        D = fallingTree.size();
        if (D > 2) {
            fallingTree.add(fallingTree.get(0));
            fallingTree.add(fallingTree.get(1));
            boolean start = true;
            for (int i = 0; i < fallingTree.size() - 1; i++) {
                int tree = fallingTree.get(i);
                int nextTree = fallingTree.get(i + 1);
                if (nextTree != (tree + 1) % N) {
                    start = true;
                    continue;
                }
                if (start) {
                    start = false;
                    continue;
                }
                E += 1;
            }
        }
        System.out.printf("%d %d %d%n", T, D, E);
        scanner.close();
    }
}
