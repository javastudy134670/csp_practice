package csp202203_3;

import java.util.*;

public class Main {

    static Scanner scanner = new Scanner(System.in);
    static int[] occupancy;
    static MyMap areaNodeMap;
    static MyMap taskNodeMap;
    static MyMap areaOfTasks;

    static {
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        occupancy = new int[n + 1];
        taskNodeMap = new MyMap();
        areaNodeMap = new MyMap();
        areaOfTasks = new MyMap();

        for (int i = 0; i < n; i++) {
            int key = scanner.nextInt();
            areaNodeMap.get(key).set(i + 1);
            areaNodeMap.get(0).set(i + 1);
        }
    }

    public static void main(String[] args) {
        int g = scanner.nextInt();
        for (int i = 0; i < g; i++) {
            int f = scanner.nextInt();
            int a = scanner.nextInt();
            int na = scanner.nextInt();
            int pa = scanner.nextInt();
            int paa = scanner.nextInt();
            int paar = scanner.nextInt();
            BitSet range = new BitSet();
            if (pa != 0) {
                areaOfTasks.get(pa).stream().forEach(each -> range.or(areaNodeMap.get(each)));
                range.and(areaNodeMap.get(na));
            } else {
                range.or(areaNodeMap.get(na));
            }
            allocateNodes(f, taskNodeMap.get(a), paar, range, taskNodeMap.get(paa), paa == a);
            BitSet tmp = new BitSet();
            Set<Integer> keys = areaNodeMap.keySet();

            for (Integer each : keys) {
                if (each == 0) continue;
                tmp.or(taskNodeMap.get(a));
                tmp.and(areaNodeMap.get(each));
                if (!tmp.isEmpty()) {
                    areaOfTasks.get(a).set(each);
                }
            }
        }
        scanner.close();
    }

    static void allocateNodes(int f, BitSet set, int paar, BitSet set1, BitSet set2, boolean self) {
        BitSet tmp = new BitSet();
        tmp.or(set1);
        tmp.andNot(set2);
        if (paar == 0 && tmp.isEmpty()) {
            //尽量满足
            tmp.or(set1);
            self = false;
        }
        if (tmp.isEmpty()) {
            for (int j = 0; j < f; j++) {
                System.out.print("0 ");
            }
            System.out.println();
            return;
        }
        List<Integer> list = new ArrayList<>();
        tmp.stream().forEach(list::add);
        int j = 0;
        if (self) {
            list.sort(comparator);
            for (int i = 0; i < list.size() && j < f; i++) {
                set.set(list.get(i));
                occupancy[list.get(i)]++;
                j++;
                System.out.printf("%d ", list.get(i));
            }
            if (j < f && paar == 0) {
                //格局打开
                tmp.or(set1);
                list.clear();
                tmp.stream().forEach(list::add);
            } else {
                while (j < f) {
                    System.out.print("0 ");
                    j++;
                }
                System.out.println();
                return;
            }
        }
        while (j < f) {
            list.sort(comparator);
            set.set(list.get(0));
            occupancy[list.get(0)]++;
            System.out.printf("%d ", list.get(0));
            j++;
            int i = 1;
            while (j < f && i < list.size() && comparator.compare(i - 1, i) > 0) {
                set.set(list.get(i));
                occupancy[list.get(i)]++;
                System.out.printf("%d ", list.get(i));
                i++;
                j++;
            }
        }
        System.out.println();
    }

    static Comparator<Integer> comparator = (o1, o2) -> {
        if (occupancy[o1] == occupancy[o2]) {
            return o1 - o2;
        }
        return occupancy[o1] - occupancy[o2];
    };

    static void deadLoop() {
        int i = 0;
        int j = 0;
        while (i == j) {
            i++;
            j++;
        }
    }
}

class MyMap extends HashMap<Integer, BitSet> {
    public BitSet get(Integer key) {
        if (!this.containsKey(key)) {
            this.put(key, new BitSet());
        }
        return super.get(key);
    }
}
