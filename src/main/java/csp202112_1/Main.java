package csp202112_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int N = scanner.nextInt();
        int[] numbers = new int[n + 2];
        for (int i = 1; i < n + 1; i++) {
            numbers[i] = scanner.nextInt();
        }
        numbers[n + 1] = N;
        int sum = 0;
        for (int i = 0; i < n + 1; i++) {
            sum += i * (numbers[i + 1] - numbers[i]);
        }
        System.out.println(sum);
    }
}
