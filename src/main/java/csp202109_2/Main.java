package csp202109_2;

import java.util.*;

/**
 * <h1>非零段划分 考察点：动态规划、差分法</h1>
 * 动态规划的需要知道阶段顺序
 * 不同的阶段就是，水位从高往低走，每个高度就是一个阶段。不同阶段的问题是，山峰的数量变化，以及发生变化之后的山峰个数。
 * 山峰的数量变化不能按阶段的顺序（从上往下）来计算，而是方便通过从左往右判断并计算，因此计算差分的思路时从左往右。
 * 这提示我们差分法求阶段性决策、变化时的取值时，采取阶段的顺序不一定是最方便的。一定要找到最方便阶段顺序。
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n + 2];
        for (int i = 1; i < n + 1; i++) {
            a[i] = scanner.nextInt();
            if (a[i] == a[i - 1]) {
                i--;
                n--;
            }
        }
        a[n + 1] = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 1; i < n + 1; i++) {
            map.put(a[i], 0);
        }
        for (int i = 1; i < n + 1; i++) {
            if (a[i] > a[i - 1] && a[i] > a[i + 1]) map.replace(a[i], map.get(a[i]) + 1);
            if (a[i] < a[i - 1] && a[i] < a[i + 1]) map.replace(a[i], map.get(a[i]) - 1);
        }
        int max = 0;
        int thing = 0;
        Integer[] b = map.keySet().toArray(new Integer[0]);
        Arrays.sort(b, Collections.reverseOrder());
        for (int i : b) {
            thing += map.get(i);
            if (thing > max) max = thing;
        }
        System.out.println(max);
        scanner.close();
    }
}
