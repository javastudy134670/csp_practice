package csp202212_3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] serialization = new int[64];
        for (int i = 0; i < 64; i++) {
            serialization[i] = scanner.nextInt();
        }
        int[] data = new int[64];
        int n = scanner.nextInt();
        int T = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            data[i] = scanner.nextInt();
        }
        data = fill(data);
        if (T != 0) {
            for (int i = 0; i < data.length; i++) {
                data[i] *= serialization[i];
            }
            if (T == 2) {
                data = cos(data);
            }
        }
        print(data);
        scanner.close();
    }

    static int[] fill(int[] data) {
        int[] result = new int[64];
        int x = 0;
        int y = 0;
        boolean up = true;
        for (int i = 0; i < data.length; ) {
            result[y * 8 + x] = data[i];
            if (up) {
                if (y == 0) {
                    x++;
                } else {
                    y++;
                }
                i++;
                if (i >= data.length) break;
                up = false;
                while (x > 0 && y < 7) {
                    result[y * 8 + x] = data[i];
                    x--;
                    y++;
                    i++;
                }
            } else {
                if (y == 7) {
                    x++;
                } else {
                    y++;
                }
                i++;
                up = true;
                while (y > 0 && x < 7) {
                    result[y * 8 + x] = data[i];
                    x++;
                    y--;
                    i++;
                }
            }
        }
        return result;
    }

    static void print(int[] data) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.printf("%d ", data[i * 8 + j]);
            }
            System.out.println();
        }
    }

    static int[] cos(int[] data) {
        int[] result = new int[64];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                double tmp = 0;
                for (int u = 0; u < 8; u++) {
                    for (int v = 0; v < 8; v++) {
                        double p = 2;
                        int a = (2 * i + 1) * u;
                        int b = (2 * j + 1) * v;
                        if ((u == 0) != (v == 0)) {
                            p = Math.sqrt(2);
                        } else if (u == 0) {
                            p = 1;
                        }
                        p *= data[8 * u + v];
                        p *= (Math.cos(Math.PI * (a + b) / 16.0) + Math.cos(Math.PI * (a - b) / 16.0));
                        tmp += p;
                    }
                }
                tmp /= 16;
                tmp += 128;
                int q = ((int) (2 * tmp) + 1) / 2;
                q = Math.min(255, q);
                q = Math.max(0, q);
                result[8 * i + j] = q;
            }
        }
        return result;
    }
}
