package csp202209_3.rewrite;

import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    static BiSet regionsBiSet = new BiSet();
    static BiSet usersBiSet = new BiSet();
    static RegionRecords regionRecords = new RegionRecords();
    static UserTravelRecords userTravelRecords = new UserTravelRecords();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            regionRecords.nextDay();
            userTravelRecords.nextDay();
            int r = scanner.nextInt();
            int m = scanner.nextInt();
            for (int j = 0; j < r; j++) {
                int key = regionsBiSet.getKey(scanner.nextInt());
                regionRecords.update(key);
            }
            for (int j = 0; j < m; j++) {
                int d = scanner.nextInt();
                int key_u = usersBiSet.getKey(scanner.nextInt());
                int key_r = regionsBiSet.getKey(scanner.nextInt());
                userTravelRecords.update(i - d, key_u, key_r);
            }
            BitSet users = new BitSet();
            BitSet persistDangerRegions = new BitSet();
            persistDangerRegions.or(regionRecords.dangerRegionsThatDay(0));
            for (int j = 0; j < 7; j++) {
                persistDangerRegions.and(regionRecords.dangerRegionsThatDay(j));
                if (persistDangerRegions.isEmpty()) break;
                BitSet tmp = new BitSet();
                tmp.or(persistDangerRegions);
                tmp.and(userTravelRecords.regionsThatDay(j));
                int finalJ = j;
                tmp.stream().forEach(region -> users.or(userTravelRecords.usersEachRegions(finalJ, region)));
            }
            System.out.printf("%d ", i);
            List<Integer> userList = new ArrayList<>();
            users.stream().forEach(each -> userList.add(usersBiSet.getValue(each)));
            userList.sort(Comparator.naturalOrder());
            userList.forEach(each -> System.out.printf("%d ", each));
            System.out.println();
        }
        scanner.close();
    }
}

class BiSet {
    HashMap<Integer, Integer> positive = new HashMap<>();
    HashMap<Integer, Integer> negative = new HashMap<>();
    int size = 0;

    int getKey(int value) {
        if (!positive.containsKey(value)) {
            positive.put(value, size);
            negative.put(size, value);
            size++;
        }
        return positive.get(value);
    }

    int getValue(int key) {
        if (key >= size) throw new IndexOutOfBoundsException();
        return negative.get(key);
    }
}

class RegionRecords {
    Queue<Pair<Integer, Integer>> queue = new LinkedBlockingQueue<>();
    Map<Integer, Integer> nums = new HashMap<>();
    BitSet[] records = new BitSet[7];
    int currentDay = -1;
    int offset = 0;

    {
        for (int i = 0; i < records.length; i++) {
            records[i] = new BitSet();
        }
    }

    void nextDay() {
        int nextOff = (offset + 6) % 7;
        records[nextOff].clear();
        records[nextOff].or(records[offset]);
        offset = nextOff;
        currentDay++;
        while (!queue.isEmpty() && queue.peek().getValue() < currentDay - 6) {
            int peek = Objects.requireNonNull(queue.poll()).getKey();
            nums.put(peek, nums.get(peek) - 1);
            if (nums.get(peek) <= 0) {
                records[offset].flip(peek);
                nums.remove(peek);
            }
        }
    }

    void update(int key) {
        queue.add(new Pair<>(key, currentDay));
        records[offset].set(key);
        if (!nums.containsKey(key)) {
            nums.put(key, 0);
        }
        nums.put(key, nums.get(key) + 1);
    }

    BitSet dangerRegionsThatDay(int daysBefore) {
        return records[(offset + daysBefore) % 7];
    }

}

class UserTravelRecords {
    UserRecord[] recordsEachDay = new UserRecord[7];
    int offset = 6;

    {
        for (int i = 0; i < recordsEachDay.length; i++) {
            recordsEachDay[i] = new UserRecord();
        }
    }

    void nextDay() {
        offset = (offset + 6) % 7;
        recordsEachDay[offset].clear();
    }

    void update(int d, int key, int r1) {
        if (d >= 7) return;
        int cur = (offset + d) % 7;
        if (!recordsEachDay[cur].containsKey(r1)) {
            recordsEachDay[cur].put(r1, new BitSet());
        }
        recordsEachDay[cur].get(r1).set(key);
    }

    BitSet usersEachRegions(int daysBefore, int r) {
        return recordsEachDay[(offset + daysBefore) % 7].get(r);
    }

    BitSet regionsThatDay(int daysBefore) {
        return recordsEachDay[(offset + daysBefore) % 7].keySet;
    }
}

class UserRecord extends HashMap<Integer, BitSet> {
    BitSet keySet = new BitSet();

    @Override
    public BitSet put(Integer key, BitSet value) {
        keySet.set(key);
        return super.put(key, value);
    }

    @Override
    public void clear() {
        keySet.clear();
        super.clear();
    }
}