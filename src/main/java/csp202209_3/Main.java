package csp202209_3;

import java.util.*;

public class Main {
    static UserTravelRecords userTravelRecord = new UserTravelRecords();
    static RegionRecords regionRecords = new RegionRecords();

    static void nextDay() {
        regionRecords.nextDay();
        userTravelRecord.nextDay();
    }

    public static void main(String[] args) {
        BitSet users = new BitSet();
        BitSet tmp = new BitSet();
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            nextDay();
            int r = scanner.nextInt();
            int m = scanner.nextInt();
            for (int j = 0; j < r; j++) {
                regionRecords.update(scanner.nextInt());
            }
            for (int j = 0; j < m; j++) {
                userTravelRecord.update(i - scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
            }
            users.clear();
            tmp.clear();
            for (int j = 6; j >= 0; j--) {
                tmp.or(regionRecords.dangerCities(j));
                int finalJ = j;
                tmp.stream().forEach(r0 -> {
                            if (userTravelRecord.recordsEachDay[finalJ].containsKey(finalJ)) {
                                users.or(userTravelRecord.usersEachRegions(finalJ, r0));
                            }
                        }
                );
            }
            System.out.printf("%d ", i);
            users.stream().sorted().forEach(user -> System.out.printf("%d ", user));
            System.out.println();
        }
        scanner.close();
    }
}

class UserTravelRecords {
    UserRecord[] recordsEachDay = new UserRecord[7];
    int offset = 6;

    {
        for (int i = 0; i < recordsEachDay.length; i++) {
            recordsEachDay[i] = new UserRecord();
        }
    }

    void nextDay() {
        offset = (offset + 6) % 7;
        recordsEachDay[offset].clear();
    }

    void update(int d, int u, int r1) {
        if (d >= 7) return;
        int cur = (offset + d) % 7;
        if (!recordsEachDay[cur].containsKey(r1)) {
            recordsEachDay[cur].put(r1, new BitSet());
        }
        recordsEachDay[cur].get(r1).set(u);
    }

    BitSet usersEachRegions(int daysBefore, int r) {
        return recordsEachDay[(offset + daysBefore) % 7].get(r);
    }
}

class UserRecord extends HashMap<Integer, BitSet> {
}

class RegionRecords {
    BitSet[] recordsEachDay = new BitSet[7];
    Queue<Integer> queue = new PriorityQueue<>();
    int offset = 6;

    {
        for (int i = 0; i < recordsEachDay.length; i++) {
            recordsEachDay[i] = new BitSet();
        }
    }

    void nextDay() {
//        while(!queue.isEmpty()&&queue.peek()){
//
//        }
        offset = (offset + 6) % 7;
        recordsEachDay[offset].clear();
    }

    void update(int r) {
        recordsEachDay[offset].set(r);
    }

    BitSet dangerCities(int d) {
        return recordsEachDay[(offset + d) % 7];
    }
}