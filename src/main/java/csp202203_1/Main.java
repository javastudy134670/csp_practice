package csp202203_1;

import java.util.Scanner;

/**
 * 初始化警告
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int result = 0;
        int[] initialized = new int[n + 1];
        initialized[0] = 1;
        for (int i = 0; i < k; i++) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            if (initialized[b] == 0) {
                result += 1;
            }
            initialized[a] = 1;
        }
        System.out.println(result);
    }
}
