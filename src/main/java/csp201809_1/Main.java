package csp201809_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n =scanner.nextInt();
        int[] array =new int[n];
        for (int i = 0; i < n; i++) {
            array[i]=scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            int left = Math.max(0,i-1);
            int right = Math.min(n-1,i+1);
            int sum=0;
            for (int j = left; j <=right ; j++) {
                sum+= array[j];
            }
            System.out.printf("%d ", sum/(right-left+1));
        }

        scanner.close();
    }
}
