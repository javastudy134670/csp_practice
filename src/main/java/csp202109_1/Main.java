package csp202109_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int length = 0;
        int level = 0;
        int max = 0;
        int min = 0;
        int tmp;
        for (int i = 0; i < n; i++) {
            tmp = scanner.nextInt();
            if (tmp != level) {
                max += level * length;
                min += level;
                level = tmp;
                length = 1;
            } else {
                length += 1;
            }
        }
        max += level * (length);
        min += level;
        System.out.println(max);
        System.out.println(min);
    }
}
