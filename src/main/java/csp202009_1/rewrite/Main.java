package csp202009_1.rewrite;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int X = scanner.nextInt();
        int Y = scanner.nextInt();
        int[] dis = new int[n];
        Integer[] index = new Integer[n];
        for (int i = 0; i < n; i++) {
            index[i] = i;
            dis[i] = sq(X - scanner.nextInt()) + sq(Y - scanner.nextInt());
        }
        Arrays.sort(index, Comparator.comparingInt(integer -> dis[integer]));
        for (int i = 0; i < 3; i++) {
            System.out.println(index[i + 1]);
        }
    }

    static int sq(int n) {
        return n * n;
    }
}
