package csp202009_1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        List<Data> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(new Data(sq(x - scanner.nextInt()) + sq(y - scanner.nextInt()), i + 1));
        }
        list.sort(Comparator.comparingInt(data -> data.dis));
        for (int i = 0; i < 3; i++) {
            System.out.println(list.get(i).i);
        }
        scanner.close();
    }

    static int sq(int n) {
        return n * n;
    }
}

class Data {
    int dis;
    int i;

    public Data(int dis, int i) {
        this.dis = dis;
        this.i = i;
    }
}
