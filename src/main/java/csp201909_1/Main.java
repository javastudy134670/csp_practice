package csp201909_1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int total = 0;
        int max = 0;
        int maxi = 0;
        for (int i = 0; i < n; i++) {
            total += scanner.nextInt();
            int sum = 0;
            for (int j = 0; j < m; j++) {
                sum -= scanner.nextInt();
            }
            total -= sum;
            if (sum > max) {
                max = sum;
                maxi = i + 1;
            }
        }
        System.out.printf("%d %d %d%n", total, maxi, max);
        scanner.close();
    }
}
