package csp201712_2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        Node node = new Node(1, null);
        Node head = node;
        for (int i = 2; i <= n; i++) {
            node.next = new Node(i, null);
            node = node.next;
        }
        Node rear = node;
        node.next = head;
        int j = 1;
        while (node.next != node) {
            if (out(j, k)) {
                node.next = node.next.next;
            } else {
                node = node.next;
            }
            j++;
        }
        System.out.println(node.n);
        scanner.close();
    }

    static boolean out(int j, int k) {
        return (j % k == 0 || j % 10 == k);
    }
}

class Node {
    Node next;
    int n;

    Node(int n, Node next) {
        this.n = n;
        this.next = next;
    }
}