package csp202303_2;

import java.util.*;

/**
 * <h1>垦田计划 考察点：二分法</h1>
 * 注意，如果选择逐步计算资源开销，其效率不一定比每一次单独重新计算资源开销更省事。
 * 因为逐步计算资源开销意味着用大量的加法，而单独重新计算时直接使用了乘法。
 * <h2>因此，我们需要找到一个天数d，它介于k和天数的最大值max之间。让d达到最小，且开销小于m就可以了。</h2>
 * 我们单独写一个计算开销的方法judge(d,resource)，当开销大于资源数时返回false。
 * 如何找到这个天数d呢？可以从d=k开始，逐个递增，直到judge返回true。这是一个可行的方法。
 * 那么同样我们可以想到二分法，去找到这个符合条件的d。熟悉二分法的话可以尝试这么做。
 *
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int resource = scanner.nextInt();
        int least = scanner.nextInt();
        int max = 0;
        Field[] fields = new Field[n];
        for (int i = 0; i < n; i++) {
            fields[i] = new Field(scanner.nextInt(), scanner.nextInt());
            if (fields[i].time > max) max = fields[i].time;
        }
        Arrays.sort(fields, Comparator.comparingInt(o -> o.time));
        int l = least;
        int r = max;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (judge(fields, mid, resource)) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        System.out.println(l);
    }

    /**
     * 判断炸开垦所需天数降到d所需要的开销是否会超标
     * @param fields 田地
     * @param d 天数
     * @param resource 资源数
     * @return 超标返回false
     */
    public static boolean judge(Field[] fields, int d, int resource) {
        for (Field field : fields) {
            int dif = field.time - d;
            if (dif > 0) {
                resource -= dif * field.cost;
                if (resource < 0) return false;
            }
        }
        return true;
    }
}

class Field {
    int time;
    int cost;

    public Field(int time, int cost) {
        this.time = time;
        this.cost = cost;
    }
}
