package csp202009_2;

import java.util.Scanner;

public class Main {
    static int xl, yd, xr, yu;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int t = scanner.nextInt();
        xl = scanner.nextInt();
        yd = scanner.nextInt();//down
        xr = scanner.nextInt();
        yu = scanner.nextInt();//up
        int passBy = 0;
        int stay = 0;
        for (int i = 0; i < n; i++) {
            int length = 0;
            boolean yellow = false;
            boolean red = false;
            for (int j = 0; j < t; j++) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                if (in(x, y)) {
                    yellow = true;
                    length++;
                    if (length >= k) {
                        red = true;
                    }
                } else {
                    length = 0;
                }
            }
            if (red) {
                stay += 1;
            }
            if (yellow) {
                passBy += 1;
            }
        }
        System.out.println(passBy);
        System.out.println(stay);
        scanner.close();
    }

    static boolean in(int x, int y) {
        return x >= xl && x <= xr && y >= yd && y <= yu;
    }
}
