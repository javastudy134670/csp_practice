package csp202206_1;

import java.util.Scanner;

/**
 * 归一化处理
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double sum = 0;
        double d = 0;
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
            sum += a[i];
            d += sq(a[i]);
        }
        double average = sum / n;
        d /= n;
        d -= average * average;
        double sqrtD = Math.sqrt(d);
        for (int i = 0; i < n; i++) {
            System.out.println((a[i] - average) / sqrtD);
        }
    }

    static int sq(int n) {
        return n * n;
    }
}
