package csp202112_3;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int w = scanner.nextInt();
        int s = scanner.nextInt();
        String str = scanner.next();
        List<Integer> list = new ArrayList<>();
        HashMap<Character, Integer> table = new HashMap<>();
        for (char c = 'A'; c <= 'Z'; c++) {
            table.put(c, c - 'A');
            table.put((char) (c - 'A' + 'a'), c - 'A');
        }
        for (char c = '0'; c <= '9'; c++) {
            table.put(c, c - '0');
        }
        byte kind = 0;// 0是 大写， 1是小写， 2 是数字
        for (char c : str.toCharArray()) {
            byte kind2 = kind(c);
            if (kind != kind2) {
                if ((kind == 0 || kind == 2) && kind2 == 1) list.add(27);
                if ((kind == 0 || kind == 1) && kind2 == 2 || kind == 2 && kind2 == 0) list.add(28);
                if (kind == 1 && kind2 == 0) {
                    list.add(28);
                    list.add(28);
                }
            }
            list.add(table.get(c));
            kind = kind2;
        }
        if (list.size() % 2 != 0) list.add(29);
        int size = list.size() / 2;
        int k;
        if (s == -1) {
            k = 0;
        } else {
            k = 1 << (s + 1);
        }
        int l = (size + 1 + k) % w == 0 ? size + 1 : ((size + 1 + k) / w + 1) * w - k;
        List<Integer> result = new ArrayList<>();
        result.add(l);
        for (int i = 0; i < size; i++) {
            result.add(list.get(2 * i) * 30 + list.get(2 * i + 1));
        }
        while (result.size() < l) {
            result.add(900);
        }
        if (k != 0) {
            Poly2 d = new Poly2();
            Poly2 g = new Poly2();
            for (int i = 0; i < l; i++) {
                d.put(i + k, Long.valueOf(result.get(l - i - 1)));
            }
            g.put(0, 1L);
            int tmpK = 3;
            for (int i = 0; i < k; i++) {
                Poly2 tmp = new Poly2();
                tmp.put(1, 1L);
                tmp.put(0, (long) -tmpK);
                tmpK *= 3;
                tmpK %= 929;
                g.multi(tmp);
            }
            d.divide(g);
            for (int i = 0; i < k; i++) {
                result.add((int) ((-d.get(k - 1 - i) % 929 + 929) % 929));
            }
        }
        result.forEach(System.out::println);
        scanner.close();
    }

    static byte kind(char c) {
        if (c >= 'A' && c <= 'Z') return 0;
        if (c >= 'a' && c <= 'z') return 1;
        return 2;
    }
}


class Poly2 extends HashMap<Integer, Long> {
    int max;

    void divide(Poly2 poly2) {
        while (max >= poly2.max) {
            long d = get(max);
            for (int i = poly2.max, j = max; i >= 0; i--, j--) {
                put(j, get(j) - d * poly2.get(i));
            }
        }
    }

    void multi(Poly2 ploy) {
        Poly2 tmp = new Poly2();
        for (Integer keys : keySet()) {
            for (Integer keys2 : ploy.keySet()) {
                tmp.put(keys2 + keys, tmp.get(keys2 + keys) + get(keys) * ploy.get(keys2));
            }
        }
        clear();
        putAll(tmp);
        max = tmp.max;
    }


    @Override
    public Long get(Object key) {
        if (!containsKey(key)) {
            return 0L;
        }
        return super.get(key);
    }

    @Override
    public Long put(Integer key, Long value) {
        if (value == 0) {
            remove(key);
            return value;
        }
        max = Math.max(max, key);
        return super.put(key, (value % 929 + 929) % 929);
    }

    @Override
    public Long remove(Object key) {
        while (!containsKey(max)) max--;
        return super.remove(key);
    }
}