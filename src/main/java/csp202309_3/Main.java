package csp202309_3;

import java.util.*;

public class Main {
    static final int n;//number of the arguments
    static final int m;
    static final Scanner scanner;

    static {
        scanner = new Scanner(System.in);
        n = scanner.nextInt();
        m = scanner.nextInt();
    }

    public static void main(String[] args) {
        scanner.nextLine();
        String boLan = scanner.nextLine();
        Node node = Node.parseNode(boLan);
        int[] values = new int[n];
        for (int i = 0; i < m; i++) {
            int index = scanner.nextInt();
            for (int j = 0; j < n; j++) {
                values[j] = scanner.nextInt();
            }
            System.out.println(node.derivative(index, values));
        }
        scanner.close();
    }
}

abstract class Node {

    static final int model = ((int) 1e9) + 7;
    Node left = null;
    Node right = null;

    public static Node parseNode(String boLan) {
        String[] inputs = boLan.split(" ");
        Stack<Node> stack = new Stack<>();
        for (String input : inputs) {
            if (input.equals("+") || input.equals("-") || input.equals("*")) {
                Node node = new Poly(input);
                node.right = stack.pop();
                node.left = stack.pop();
                stack.push(node);
            } else {
                if (input.charAt(0) == 'x') {
                    stack.push(new EndNode(Integer.parseInt(input.substring(1))));
                } else {
                    stack.push(new ConstNode(Integer.parseInt(input)));
                }
            }
        }
        return stack.peek();
    }

    public static int normalize(long value) {
        while (value < 0) value += model;
        return (int) (value % model);
    }

    abstract public int result(int[] values);

    abstract public int derivative(int index, int[] values);
}

class Poly extends Node {
    final String type;

    Poly(String type) {
        this.type = type;
    }

    @Override
    public int result(int[] values) {
        int l = left.result(values);
        int r = right.result(values);
        switch (type) {
            case "+":
                return normalize(l + r);
            case "-":
                return normalize(l - r);
            case "*":
                return normalize((long) l * r);
            default:
                throw new RuntimeException("no way!");
        }
    }

    @Override
    public int derivative(int index, int[] values) {
        int l_result = left.result(values);
        int l_derivative = left.derivative(index, values);
        int r_result = right.result(values);
        int r_derivative = right.derivative(index, values);
        switch (type) {
            case "+":
                return normalize(l_derivative + r_derivative);
            case "-":
                return normalize(l_derivative - r_derivative);
            case "*":
                return normalize((long) l_derivative * r_result + (long) l_result * r_derivative);
            default:
                throw new RuntimeException("no way2!");
        }
    }

    @Override
    public String toString() {
        return String.format("(%s %s %s)", left.toString(), type, right.toString());
    }
}

class EndNode extends Node {
    final int index;

    EndNode(int index) {
        this.index = index;
    }

    @Override
    public int result(int[] values) {
        return normalize(values[index - 1]);
    }

    @Override
    public int derivative(int index, int[] values) {
        if (index == this.index) return 1;
        return 0;
    }

    @Override
    public String toString() {
        return String.format("x%d", index);
    }
}

class ConstNode extends Node {
    final int constNum;

    ConstNode(int constNum) {
        this.constNum = constNum;
    }

    @Override
    public int result(int[] values) {
        return normalize(constNum);
    }

    @Override
    public int derivative(int index, int[] values) {
        return 0;
    }

    @Override
    public String toString() {
        return Integer.toString(constNum);
    }
}