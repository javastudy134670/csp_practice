package csp201712_1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Integer[] nums = new Integer[n];
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }
        Arrays.sort(nums, Comparator.reverseOrder());
        for (int i = 0; i < n - 1; i++) {
            nums[i] -= nums[i + 1];
        }
        nums[n - 1] = Integer.MAX_VALUE;
        Arrays.sort(nums);
        System.out.println(nums[0]);
    }
}
