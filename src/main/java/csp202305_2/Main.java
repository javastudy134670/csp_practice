package csp202305_2;

import java.util.Scanner;

/**
 * <h1>矩阵运算 考察点：矩阵运算结合律、审查计算过程开销</h1>
 * 需要注意的是矩阵具有结合律，想办法让中间产物尽可能的小。
 * 具体做法是先计算KTxV，再计算Q
 */
public class Main {
    static int n;
    static int d;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        d = scanner.nextInt();
        // 在Java中，二维矩阵可以用一维数组来存储Q[i][j]即Q[i*n+j]，其中n为列数。
        int[] Q = new int[n * d];
        int[] K = new int[n * d];
        int[] V = new int[n * d];
        for (int i = 0; i < Q.length; i++) {
            Q[i] = scanner.nextInt();
        }
        for (int i = 0; i < K.length; i++) {
            K[i] = scanner.nextInt();
        }
        for (int i = 0; i < V.length; i++) {
            V[i] = scanner.nextInt();
        }
        long[] tmp = new long[d * d];//注意细节，使用long类型来满足需要
        for (int i = 0; i < d; i++) {
            for (int j = 0; j < d; j++) {
                for (int k = 0; k < n; k++) {
                    tmp[i * d + j] += (long) K[k * d + i] * V[k * d + j];
                }
            }
        }
        long[] result = new long[n * d];
        for (int i = 0; i < n; i++) {
            int times = scanner.nextInt();//在这里输入W，可以减少中间产物的大小
            for (int j = 0; j < d; j++) {
                for (int k = 0; k < d; k++) {
                    result[i * d + j] += Q[i * d + k] * tmp[k * d + j];
                }
                result[i * d + j] *= times;
                System.out.print(result[i * d + j]);
                if (j < d - 1) {
                    System.out.print(' ');
                } else {
                    System.out.println();
                }
            }
        }
        scanner.close();
    }
}
