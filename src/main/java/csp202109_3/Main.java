package csp202109_3;

import java.util.*;

public class Main {
    static List<Group> groups = new ArrayList<>();
    static Neuron[] neurons;
    static HashMap<Integer, Synapse> synapse = new HashMap<>();
    static int[] dash;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int S = scanner.nextInt();
        int P = scanner.nextInt();
        int T = scanner.nextInt();
        double dt = scanner.nextDouble();
        neurons = new Neuron[N];
        int n = 0;
        while (n < N) {
            int RN = scanner.nextInt();
            double u = scanner.nextDouble();
            double v = scanner.nextDouble();
            double a = scanner.nextDouble();
            double b = scanner.nextDouble();
            double c = scanner.nextDouble();
            double d = scanner.nextDouble();
            groups.add(new Group(a, b, c, d));
            int group = groups.size() - 1;
            for (int i = 0; i < RN; i++, n++) {
                neurons[n] = new Neuron(u, v, group);
            }
        }
        dash = new int[P];
        for (int i = 0; i < P; i++) {
            dash[i] = scanner.nextInt();
        }
        for (int i = 0; i < S; i++) {
            synapse.put(scanner.nextInt(), new Synapse(scanner.nextInt(), scanner.nextDouble(), scanner.nextInt()));
        }
        scanner.close();
    }


}

class Neuron {
    double v;
    double u;
    int group;


    public Neuron(double u, double v, int group) {
        this.u = u;
        this.v = v;
        this.group = group;
    }

}

class Synapse {
    int v;
    double w;
    int D;

    public Synapse(int v, double w, int D) {
        this.v = v;
        this.w = w;
        this.D = D;
    }
}

class Group {
    final double a;
    final double b;
    final double c;
    final double d;
    MyMap I = new MyMap();

    Group(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
}

class MyMap extends HashMap<Integer, Double> {
    @Override
    public Double get(Object key) {
        if (!containsKey(key)) return 0.0;
        return super.get(key);
    }

}

class Rand {
    static long next = 1;

    static int myRand() {
        next = next * 1103515245 + 12345;
        return (int) ((Long.divideUnsigned(next, 65536)) % 32768);
    }
}